
<!-- Footer -->
<footer class="footer">
    <div class="container">
        <div class="has-text-centered">
            <div class="has-text-centered">
                <img class="small-footer-logo" src="assets/images/logos/bulkit-logo-g.png" alt="">
            </div>
        </div>
        <div class="has-text-centered">
            <span class="more-info-company">Startup and SaaS template</span>
        </div>
    </div>
</footer>
<!-- /Footer -->

<!-- Right sidebar quickview -->
<div id="main-quickview" class="quickview">
    <!-- Quickview header -->
    <header class="quickview-header is-secondary">
        <p class="title">Messages</p>
        <div class="cross-container" data-dismiss="quickview">
            <span class="top"></span>
            <span class="bottom"></span>
        </div>
    </header>
    <!-- /Quickview header -->

    <!-- Quickview body -->
    <div class="quickview-body">
        <div class="quickview-block">
            <!-- Quickview tabs -->
            <div class="navigation-tabs translated-tabs simple-tabs">
                <div class="tabs is-fullwidth">
                    <ul>
                        <li class="is-active" data-tab="user-directory"><a><i class="sl sl-icon-people"></i></a></li>
                        <li data-tab="reminders"><a><i class="sl sl-icon-clock"></i></a></li>
                        <li data-tab="quickview-settings"><a><i class="sl sl-icon-settings"></i></a></li>
                    </ul>
                </div>

                <!-- User directory tab -->
                <div id="user-directory" class="navtab-content is-active">
                    <div class="panel-title">
                        <h3>USER DIRECTORY</h3>
                        <i class="material-icons ml-auto">more_vert</i>
                    </div>

                    <!-- Seach field -->
                    <div class="field">
                        <div class="searchbox control has-icons-left">
                            <input class="input" type="text" placeholder="Search users">
                            <span class="icon is-left">
                                        <i class="sl sl-icon-magnifier"></i>
                                    </span>
                        </div>
                    </div>
                    <!-- /Seach field -->

                    <!-- User list -->
                    <ol class="user-list">
                        <!-- User -->
                        <li class="quickview-trigger" data-quickview="chat-quickview">
                            <div class="status-dot"></div>
                            <img src="https://via.placeholder.com/250x250" alt="">
                            <div class="user-status">
                                <div class="name">Anthony Briggs</div>
                                <div class="status">Offline</div>
                            </div>
                        </li>
                        <!-- User -->
                        <li>
                            <div class="status-dot is-online"></div>
                            <img src="https://via.placeholder.com/250x250" alt="">
                            <div class="user-status">
                                <div class="name">Benedict Shaeffer</div>
                                <div class="status">Online</div>
                            </div>
                        </li>
                        <!-- User -->
                        <li>
                            <div class="status-dot is-busy"></div>
                            <img src="https://via.placeholder.com/250x250" alt="">
                            <div class="user-status">
                                <div class="name">Nicole Ferrier</div>
                                <div class="status">Busy</div>
                            </div>
                        </li>
                        <!-- User -->
                        <li>
                            <div class="status-dot"></div>
                            <img src="https://via.placeholder.com/250x250" alt="">
                            <div class="user-status">
                                <div class="name">Henry Rodstein</div>
                                <div class="status">Offline</div>
                            </div>
                        </li>
                        <!-- User -->
                        <li>
                            <div class="status-dot is-online"></div>
                            <img src="https://via.placeholder.com/250x250" alt="">
                            <div class="user-status">
                                <div class="name">Marjory Cambell</div>
                                <div class="status">Online</div>
                            </div>
                        </li>
                        <!-- User -->
                        <li>
                            <div class="status-dot is-online"></div>
                            <img src="https://via.placeholder.com/250x250" alt="">
                            <div class="user-status">
                                <div class="name">Stella Roberts</div>
                                <div class="status">Online</div>
                            </div>
                        </li>
                        <!-- User -->
                        <li>
                            <div class="status-dot is-busy"></div>
                            <img src="https://via.placeholder.com/250x250" alt="">
                            <div class="user-status">
                                <div class="name">Sayid Jabrane</div>
                                <div class="status">Busy</div>
                            </div>
                        </li>
                        <!-- User -->
                        <li class="giant-pb">
                            <div class="status-dot"></div>
                            <img src="https://via.placeholder.com/250x250" alt="">
                            <div class="user-status">
                                <div class="name">Sally Mitchells</div>
                                <div class="status">Offline</div>
                            </div>
                        </li>
                    </ol>
                    <!-- /User list -->
                </div>
                <!-- /User directory tab -->
                <!-- Reminders tab -->
                <div id="reminders" class="navtab-content">
                    <div class="panel-title">
                        <h3>REMINDERS</h3>
                        <i class="material-icons modal-trigger ml-auto" data-toggle="tooltip" data-placement="left"
                           data-title="Add reminder" data-modal="add-reminder-modal">add</i>
                    </div>
                    <!-- Reminders list -->
                    <ol class="reminders-list">
                        <!-- Reminders -->
                        <li>
                            <!-- Reminder -->
                            <div class="reminder">
                                <div class="info">
                                    <i class="fa fa-circle is-low"></i>
                                    <span><i class="im im-icon-Calendar-4"></i></span>
                                    <span class="date"> Today</span>
                                </div>
                                <div class="reminder-content">
                                    Review the number of subscriptions before ICO
                                </div>
                                <div class="reminder-controls">
                                    <i class="sl sl-icon-settings" data-toggle="tooltip" data-placement="top"
                                       data-title="Edit"></i>
                                    <i class="sl sl-icon-trash" data-toggle="tooltip" data-placement="top"
                                       data-title="Delete"></i>
                                </div>
                            </div>
                            <!-- Reminder -->
                            <div class="reminder">
                                <div class="info">
                                    <i class="fa fa-circle is-low"></i>
                                    <span><i class="im im-icon-Calendar-4"></i></span>
                                    <span class="date"> 2 weeks left</span>
                                </div>
                                <div class="reminder-content">
                                    Clean up database before migration to production server
                                </div>
                                <div class="reminder-controls">
                                    <i class="sl sl-icon-settings" data-toggle="tooltip" data-placement="top"
                                       data-title="Edit"></i>
                                    <i class="sl sl-icon-trash" data-toggle="tooltip" data-placement="top"
                                       data-title="Delete"></i>
                                </div>
                            </div>
                            <!-- Reminder -->
                            <div class="reminder">
                                <div class="info">
                                    <i class="fa fa-circle is-high"></i>
                                    <span><i class="im im-icon-Calendar-4"></i></span>
                                    <span class="date"> 1 month left</span>
                                </div>
                                <div class="reminder-content">
                                    Rewrite user model scripting before tests
                                </div>
                                <div class="reminder-controls">
                                    <i class="sl sl-icon-settings" data-toggle="tooltip" data-placement="top"
                                       data-title="Edit"></i>
                                    <i class="sl sl-icon-trash" data-toggle="tooltip" data-placement="top"
                                       data-title="Delete"></i>
                                </div>
                            </div>
                        </li>
                    </ol>
                    <!-- /Reminders list -->
                </div>
                <!-- /Reminders tab -->
                <!-- Settings tab -->
                <div id="quickview-settings" class="navtab-content">
                    <!-- Header -->
                    <div class="panel-title">
                        <h3>SETTINGS</h3>
                        <i class="material-icons ml-auto">more_vert</i>
                    </div>
                    <!-- Header -->

                    <!-- Settings -->
                    <ol class="settings-list">
                        <!-- Busy mode toggle -->
                        <li>
                            <div class="setting-header">
                                <div class="setting-title">Busy mode</div>
                                <div class="setting-control">
                                    <div class="field">
                                        <input id="busySwitch" type="checkbox" name="busySwitch"
                                               class="switch is-outlined is-warning">
                                        <label for="busySwitch"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="setting-description">
                                Activate to set your status to busy. While you are busy, other users can't send you
                                direct messages.
                            </div>
                        </li>
                        <!-- /Busy mode toggle -->

                        <!-- Setting -->
                        <li>
                            <div class="setting-header">
                                <div class="setting-title">Notifications</div>
                                <div class="setting-control">
                                    <div class="field">
                                        <input id="notificationsSwitch" type="checkbox" name="notificationsSwitch"
                                               checked class="switch is-outlined is-secondary">
                                        <label for="notificationsSwitch"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="setting-description">
                                Activate to enabla activity notifications. You can find more notifications settings in
                                the general settings.
                            </div>
                        </li>
                        <!-- Setting -->
                        <li>
                            <div class="setting-header">
                                <div class="setting-title">Push Notifications</div>
                                <div class="setting-control">
                                    <div class="field">
                                        <input id="notificationsPushSwitch" type="checkbox"
                                               name="notificationsPushSwitch" checked
                                               class="switch is-outlined is-primary">
                                        <label for="notificationsPushSwitch"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="setting-description">
                                Activate to enable push notifications. You can find more notifications settings in the
                                general settings.
                            </div>
                        </li>
                    </ol>
                    <!-- /Settings -->
                </div>
                <!-- /Settings tab -->            </div>
            <!-- /Quickview tabs -->
        </div>
    </div>
    <!-- /Quickview body -->

    <!-- Quickview footer -->
    <footer class="quickview-footer"></footer>
    <!-- /Quickview footer -->
</div>        <!-- Chat quickview -->
<div id="chat-quickview" class="quickview custom-quickview chat-quickview">
    <header class="quickview-header is-secondary">
        <p class="title">Anthony Briggs <span class="tag is-outlined is-light">2 new</span></p>
        <div class="cross-container quickview-close" data-quickview="chat-quickview">
            <span class="top"></span>
            <span class="bottom"></span>
        </div>
    </header>

    <!-- Quickview message body -->
    <div class="quickview-body">
        <div id="message-container" class="message-container">
            <div class="divider">
                <span class="before-divider"></span>
                <div class="children">Yesterday</div>
                <span class="after-divider"></span>
            </div>
            <div class="chat-message to">
                <div class="bubble-wrapper">
                    <div class="timestamp">02:48 pm</div>
                    <div class="chat-bubble">
                        Hello Anthony, did you finish the Hackman project's report ?
                    </div>
                </div>
                <img src="https://via.placeholder.com/250x250" alt="">
            </div>
            <div class="chat-message to">
                <div class="bubble-wrapper">
                    <div class="timestamp">02:48 pm</div>
                    <div class="chat-bubble">
                        I need it for some paper work.
                    </div>
                </div>
                <img src="https://via.placeholder.com/250x250" alt="">
            </div>
            <div class="chat-message from">
                <img src="https://via.placeholder.com/250x250" alt="">
                <div class="bubble-wrapper">
                    <div class="timestamp">02:49 pm</div>
                    <div class="chat-bubble">
                        The Hackman report ? Iam almost done with it
                    </div>
                </div>
            </div>
            <div class="divider mt-40">
                <span class="before-divider"></span>
                <div class="children">Today</div>
                <span class="after-divider"></span>
            </div>
            <div class="chat-message from">
                <img src="https://via.placeholder.com/250x250" alt="">
                <div class="bubble-wrapper">
                    <div class="timestamp">11:37 pm</div>
                    <div class="chat-bubble">
                        Did you reveive the report in your inbox ? I sent it to you this morning
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Quickview message body -->

    <!-- Message input -->
    <footer class="quickview-footer">
        <div class="message-input">
            <textarea id="chat-input" class="" rows="1" placeholder="Send a message ..."></textarea>
            <div class="message-options">
                <div class="emoji-button"></div>
                <div class="attach-button"></div>
            </div>
        </div>
    </footer>
    <!-- /Message input -->
</div>
<!-- /Chat quickview -->        <!-- Search Modal -->
<div id="search-modal" class="modal modal-lg modal-hero">
    <div class="modal-background"></div>
    <div class="modal-content">
        <div class="control-material has-icon is-light">
            <input class="material-input" type="text" placeholder="Search..." required autofocus>
            <i class="material-icons">search</i>
            <span class="material-highlight"></span>
            <span class="bar"></span>
        </div>
    </div>
    <button class="modal-close is-large is-hidden" aria-label="close"></button>
</div>
<!-- Search modal -->        <!-- Reminder Modal -->
<div id="add-reminder-modal" class="modal modal-sm modal-hero">
    <div class="modal-background"></div>
    <div class="modal-content">
        <div class="flex-card simple-shadow">
            <div class="card-body">
                <div class="content">
                    <h2 class="has-text-centered dark-text pb-20">New Reminder</h2>
                    <!-- New reminder form -->
                    <form>
                        <div class="field mb-10">
                            <label class="label dark-text font-w-500">Remind me before</label>
                            <div class="control has-icons-right">
                                <input data-toggle="datepicker" type="text" class="input is-medium"
                                       placeholder="select a date ...">
                                <span class="icon is-right is-medium">
                                            <i class="im im-icon-Calendar-4"></i>
                                        </span>
                            </div>
                        </div>
                        <div class="field mb-10">
                            <div class="control has-icons-left">
                                <input type="text" class="input timepicker-24 is-medium" placeholder="set time">
                                <span class="icon is-left is-medium">
                                            <i class="sl sl-icon-clock"></i>
                                        </span>
                            </div>
                        </div>
                        <div class="field mb-10">
                            <label class="label dark-text font-w-500">Priority</label>
                            <div class="control">
                                <select class="chosen-select" data-placeholder="Select a priority">
                                    <option label="Priority"></option>
                                    <option>Low</option>
                                    <option>Medium</option>
                                    <option>High</option>
                                </select>
                            </div>
                        </div>
                        <div class="field mb-10">
                            <label class="label dark-text font-w-500">Remind me what ?</label>
                            <div class="control">
                                <textarea class="textarea is-grow is-secondary-focus" rows="5"
                                          placeholder="Type something relevant ..."></textarea>
                            </div>
                        </div>
                        <div class="has-text-right mt-30">
                            <button type="submit"
                                    class="button btn-dash secondary-btn no-lh rounded is-fullwidth is-raised ripple">
                                Create reminder
                            </button>
                        </div>
                    </form>
                    <!-- /New reminder form -->
                </div>
            </div>
        </div>
    </div>
    <button class="modal-close is-large is-hidden" aria-label="close"></button>
</div>
<!-- Reminder Modal -->        <!-- Concatenated jQuery and plugins -->
<script src="{{ URL::asset('js/bulkit.js') }}"></script>
<script src="{{ URL::asset('js/bulkit.js') }}assets/js/app.js"></script>

<!-- Bulkit js -->
<script src="{{ URL::asset('js/components-modals.js') }}"></script>
<script src="{{ URL::asset('js/components-quickview.js') }}"></script>
<script src="{{ URL::asset('js/common.js') }}"></script>
<script src="{{ URL::asset('js/dashboard.js') }}"></script>

<!-- Extensions js -->
<script src="{{ URL::asset('js/projects.js') }}"></script>
<script src="{{ URL::asset('js/datatable.js') }}"></script>
</body>
</html>
