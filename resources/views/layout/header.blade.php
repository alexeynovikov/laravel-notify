<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title> Dashkit :: Home</title>

    <!--Core CSS -->
    <link rel="stylesheet" href="{{ URL::asset('css/bulma.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/bulkit.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/dashboard.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/icons.min.css') }}">

</head>
<body>
<div class="pageloader"></div>
<div class="infraloader is-active"></div>
