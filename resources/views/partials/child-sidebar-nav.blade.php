<div class="child-menu-inner">
    <!-- Child menu header -->
    <ul>
        <li class="child-header">
            <div class="sidebar-title">Navigation</div>
            <a id="close-child-menu" href="#" class="menu-wrapper">
                            <span class="icon-box-toggle ef-4">
                                <span class="rotate">
                                    <i class="icon-line-top">  	 </i>
                                    <i class="icon-line-center">  </i>
                                    <i class="icon-line-bottom">  </i>
                                </span>
                            </span>
            </a>
        </li>
    </ul>
    <!-- /Child menu header -->

    <!-- Sidebar menu 1 -->
    <ul id="dashboard-menu" class="sidebar-menu is-active animated preFadeInRIght fadeInRight">
        <li class="have-children"><a class="parent-link" href="#"><span class="material-icons">dashboard</span>Dashboard</a>
            <ul>
                <li><a href="dashboard.html">Main layout</a></li>
                <li><a href="dashboard-blank.html">Blank page</a></li>
                <li><a href="dashboard-login.html">Login page</a></li>
            </ul>
        </li>
        <li class="have-children"><a class="parent-link" href="#"><span class="material-icons">insert_chart</span>Analytics</a>
            <ul>
                <li><a href="dashboard-chartjs.html">Chart js</a></li>
                <li><a href="dashboard-billboardjs.html">Billboard js</a></li>
                <li><a href="dashboard-peityjs.html">Peity js</a></li>
            </ul>
        </li>
        <li class="have-children"><a class="parent-link" href="#"><span
                        class="material-icons">developer_board</span>Widgets</a>
            <ul>
                <li><a href="dashboard-widgets-data.html">Data widgets</a></li>
                <li><a href="dashboard-widgets-social.html">Social widgets</a></li>
            </ul>
        </li>
        <li class="have-children"><a class="parent-link" href="#"><span class="material-icons">view_agenda</span>Feed</a>
            <ul>
                <li><a href="dashboard-feed.html">Social feed</a></li>
                <li><a href="dashboard-feed-post.html">Post Item</a></li>
            </ul>
        </li>
        <li class="have-children"><a class="parent-link" href="#"><span class="material-icons">invert_colors</span>Styles</a>
            <ul>
                <li><a href="/">Light sidebar</a></li>
                <li><a href="dashboard-dark-nav.html">Dark sidebar</a></li>
            </ul>
        </li>
    </ul>
    <!-- /Sidebar menu 1 -->
    <!-- Sidebar menu 2 -->
    <ul id="documents-menu" class="sidebar-menu animated preFadeInRIght fadeInRight">
        <li class="have-children"><a class="parent-link" href="#"><span
                        class="material-icons">insert_drive_file</span>Documents</a>
            <ul>
                <li><a href="javascript:void(0);">Document list <span
                                class="sl sl-icon-lock warning-text ml-10"></span></a></li>
                <li><a href="javascript:void(0);">Document details <span
                                class="sl sl-icon-lock warning-text ml-10"></span></a></li>
                <li><a href="javascript:void(0);">Invoice <span
                                class="sl sl-icon-lock warning-text ml-10"></span></a></li>
            </ul>
        </li>
        <li class="have-children"><a class="parent-link" href="#"><span class="material-icons">assignment</span>Projects</a>
            <ul>
                <li><a href="dashboard-project-list.html">Project list <span
                                class="sl sl-icon-lock-open success-text ml-10"></span></a></li>
                <li><a href="dashboard-project-details.html">Project details <span
                                class="sl sl-icon-lock-open success-text ml-10"></span></a></li>
            </ul>
        </li>
        <li class="have-children"><a class="parent-link" href="#"><span
                        class="material-icons">assignment_turned_in</span>Tasks</a>
            <ul>
                <li><a href="dashboard-project-task.html">Single Task <span
                                class="sl sl-icon-lock-open success-text ml-10"></span></a></li>
                <li><a href="dashboard-project-mytasks.html">My Tasks<span
                                class="sl sl-icon-lock-open success-text ml-10"></span></a></li>
                <li><a href="javascript:void(0);">Kanban board <span
                                class="sl sl-icon-lock warning-text ml-10"></span></a></li>
            </ul>
        </li>
        <li class="have-children"><a class="parent-link" href="#"><span class="material-icons">library_books</span>Forms</a>
            <ul>
                <li><a href="javascript:void(0);">Step form <span class="sl sl-icon-lock warning-text ml-10"></span></a>
                </li>
                <li><a href="javascript:void(0);">Steps <span class="sl sl-icon-lock warning-text ml-10"></span></a>
                </li>
            </ul>
        </li>
    </ul>
    <!-- /Sidebar menu 2 -->
    <!-- Sidebar menu 3 -->
    <ul id="business-menu" class="sidebar-menu animated preFadeInRIght fadeInRight">
        <li class="have-children"><a class="parent-link" href="#"><span class="material-icons">group</span>Contacts</a>
            <ul>
                <li><a href="javascript:void(0);">Contact list <span
                                class="sl sl-icon-lock warning-text ml-10"></span></a></li>
                <li><a href="javascript:void(0);">Contact grid <span
                                class="sl sl-icon-lock warning-text ml-10"></span></a></li>
                <li><a href="javascript:void(0);">Contact details <span
                                class="sl sl-icon-lock warning-text ml-10"></span></a></li>
            </ul>
        </li>
        <li class="have-children"><a class="parent-link" href="#"><span class="material-icons">person_add</span>Leads</a>
            <ul>
                <li><a href="javascript:void(0);">Leads list <span
                                class="sl sl-icon-lock warning-text ml-10"></span></a></li>
                <li><a href="javascript:void(0);">Add lead <span class="sl sl-icon-lock warning-text ml-10"></span></a>
                </li>
            </ul>
        </li>
        <li class="have-children"><a class="parent-link" href="#"><span class="material-icons">attach_money</span>Deals</a>
            <ul>
                <li><a href="javascript:void(0);">Deals list <span
                                class="sl sl-icon-lock warning-text ml-10"></span></a></li>
                <li><a href="javascript:void(0);">Single Deal <span
                                class="sl sl-icon-lock warning-text ml-10"></span></a></li>
            </ul>
        </li>
    </ul>
    <!-- /Sidebar menu 3 -->
    <!-- Sidebar menu 4 -->
    <ul id="misc-menu" class="sidebar-menu animated preFadeInRIght fadeInRight">
        <li class="have-children"><a class="parent-link" href="#"><span class="material-icons">grid_on</span>Datatables</a>
            <ul>
                <li><a href="dashboard-datatable-basic.html">Basic Datatable <span
                                class="sl sl-icon-lock-open success-text ml-10"></span></a></li>
                <li><a href="dashboard-datatable-variations.html">Variations <span
                                class="sl sl-icon-lock-open success-text ml-10"></span></a></li>
                <li><a href="dashboard-datatable-advanced.html">Advanced Example <span
                                class="sl sl-icon-lock-open success-text ml-10"></span></a></li>
            </ul>
        </li>
        <li class="have-children"><a class="parent-link" href="#"><span class="material-icons">speaker_notes</span>Faq</a>
            <ul>
                <li><a href="javascript:void(0);">Faq home <span class="sl sl-icon-lock warning-text ml-10"></span></a>
                </li>
                <li><a href="javascript:void(0);">Faq article <span
                                class="sl sl-icon-lock warning-text ml-10"></span></a></li>
            </ul>
        </li>
        <li class="have-children"><a class="parent-link" href="#"><span class="material-icons">thumbs_up_down</span>Forum</a>
            <ul>
                <li><a href="javascript:void(0);">Forum home <span
                                class="sl sl-icon-lock warning-text ml-10"></span></a></li>
                <li><a href="javascript:void(0);">Forum thread <span
                                class="sl sl-icon-lock warning-text ml-10"></span></a></li>
            </ul>
        </li>
        <li class="have-children"><a class="parent-link" href="#"><span
                        class="material-icons">email</span>Mailbox</a>
            <ul>
                <li><a href="javascript:void(0);">Inbox <span class="sl sl-icon-lock warning-text ml-10"></span></a>
                </li>
                <li><a href="javascript:void(0);">Compose <span
                                class="sl sl-icon-lock warning-text ml-10"></span></a></li>
            </ul>
        </li>
    </ul>
    <!-- /Sidebar menu 4 -->
    <!-- Sidebar menu 5 -->
    <ul id="settings-menu" class="sidebar-menu animated preFadeInRIght fadeInRight">
        <li class="have-children"><a class="parent-link" href="#"><span class="material-icons">settings</span>Global
                settings</a>
            <ul>
                <li><a href="javascript:void(0);">Preferences <span
                                class="sl sl-icon-lock warning-text ml-10"></span></a></li>
                <li><a href="javascript:void(0);">Change password <span
                                class="sl sl-icon-lock warning-text ml-10"></span></a></li>
            </ul>
        </li>
        <li class="have-children"><a class="parent-link" href="#"><span class="material-icons">account_circle</span>User
                settings</a>
            <ul>
                <li><a href="javascript:void(0);">User list <span class="sl sl-icon-lock warning-text ml-10"></span></a>
                </li>
                <li><a href="javascript:void(0);">Add user <span class="sl sl-icon-lock warning-text ml-10"></span></a>
                </li>
                <li><a href="javascript:void(0);">Access rights <span
                                class="sl sl-icon-lock warning-text ml-10"></span></a></li>
            </ul>
        </li>
        <li class="have-children"><a class="parent-link" href="#"><span class="material-icons">domain</span>Organizations</a>
            <ul>
                <li><a href="javascript:void(0);">Organizations list <span
                                class="sl sl-icon-lock warning-text ml-10"></span></a></li>
                <li><a href="javascript:void(0);">Add organization <span
                                class="sl sl-icon-lock warning-text ml-10"></span></a></li>
            </ul>
        </li>
    </ul>
    <!-- /Sidebar menu 5 -->    </div>
