<div class="container is-fluid">
    <!-- Nav left -->
    <div class="navbar-brand">
        <!-- Logout button -->
        <div class="navbar-item nav-icon logout-button">
            <i class="sl sl-icon-power"></i>
        </div>
        <!-- /Logout button -->

        <!-- Reader mode switch -->
        <div class="navbar-item reader-switch is-hidden-desktop is-hidden-tablet">
            <div class="field">
                <input id="reader-mode-switch" type="checkbox" name="reader-mode-switch"
                       class="switch is-outlined is-primary is-small">
                <label id="reader-mode-toggle" for="reader-mode-switch"></label>
            </div>
        </div>
        <!-- Reader mode switch -->

        <!-- Collaborators list (hidden on mobile) -->
        <div class="navbar-item is-hidden-mobile">
            <div class="collaborators">
                <div class="face">
                    <img src="https://via.placeholder.com/250x250">
                </div>
                <div class="face">
                    <img src="https://via.placeholder.com/250x250">
                </div>
                <div class="face">
                    <img src="https://via.placeholder.com/250x250">
                </div>
                <div class="face">
                    <img src="https://via.placeholder.com/250x250">
                </div>
                <div class="face is-fake">+4</div>
            </div>
        </div>
        <!-- /Quick search -->

        <!-- Global search (visible on mobile only) -->
        <div class="navbar-item nav-icon search-icon modal-trigger is-hidden-desktop is-hidden-tablet"
             data-modal="search-modal">
            <i class="sl sl-icon-magnifier"></i>
        </div>
        <!-- /Global search -->

        <!-- Right sidebar trigger (visible on mobile only)  -->
        <div class="navbar-item nav-icon chat-button is-hidden-desktop is-hidden-tablet" data-show="quickview"
             data-target="main-quickview">
            <i class="im im-icon-Speach-Bubble11"></i>
        </div>
        <!-- /Right sidebar trigger -->
    </div>
    <!-- /Nav left -->

    <!-- Nav right -->
    <div class="navbar-menu">
        <div class="navbar-end">

            <!-- Notifications dropdown (hidden on mobile) -->
            <div class="navbar-item drop-pop is-centered nav-icon">
                <a href="#" class="nav-inner">
                    <i class="sl sl-icon-bell"><span class="new-circle gelatine"></span></i>
                </a>
                <div class="drop-wrapper notifications-drop">
                    <div class="drop-inner has-arrow">
                        <div class="notifications-header has-text-centered">
                            <h3>Notifications</h3>
                        </div>
                        <div class="notifications-body">
                            <ul class="notifications-list">
                                <li>
                                    <i class="sl sl-icon-heart"></i>
                                    <div class="notification-content">
                                        <img src="https://via.placeholder.com/250x250" alt="">
                                        <div class="notification-text">
                                            <div class="text">
                                                <b>Henry</b> liked your <b>comment</b>.
                                            </div>
                                            <div class="timestamp">
                                                23 minutes ago
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <i class="sl sl-icon-check"></i>
                                    <div class="notification-content">
                                        <img src="https://via.placeholder.com/250x250" alt="">
                                        <div class="notification-text">
                                            <div class="text">
                                                <b>Marjory</b> followed you.
                                            </div>
                                            <div class="timestamp">
                                                1 hour ago
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <i class="sl sl-icon-heart"></i>
                                    <div class="notification-content">
                                        <img src="https://via.placeholder.com/250x250" alt="">
                                        <div class="notification-text">
                                            <div class="text">
                                                <b>Marc</b> commented one of your tasks.
                                            </div>
                                            <div class="timestamp">
                                                Yesterday
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Notifications dropdown -->
            <!-- Messages dropdown (hidden on mobile) -->
            <div class="navbar-item drop-pop is-centered nav-icon">
                <a href="#" class="nav-inner">
                    <i class="sl sl-icon-envelope-open"><span class="new-circle gelatine"></span></i>
                </a>
                <div class="drop-wrapper emails-drop">
                    <div class="drop-inner has-arrow">
                        <div class="emails-header has-text-centered">
                            <h3>Messages</h3>
                        </div>
                        <div class="emails-body">
                            <ul class="emails-list">
                                <li>
                                    <img src="https://via.placeholder.com/250x250" alt="">
                                    <div class="email-content">

                                        <div class="email-text">
                                            <div class="text">
                                                Hello, after our last meetings, it seems that some goals were...
                                            </div>
                                            <!--div class="timestamp">
                                                Nov 09, 14:18
                                            </div-->
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="fake-avatar is-secondary">B</span>
                                    <div class="email-content">
                                        <div class="email-text">
                                            <div class="text is-read">
                                                We did not receive the order yet, can you please check if it has...
                                            </div>
                                            <!--div class="timestamp">
                                                Nov 05, 13:31
                                            </div-->
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="fake-avatar is-primary">J</span>
                                    <div class="email-content">
                                        <div class="email-text">
                                            <div class="text is-read">
                                                My boss was very grateful that you found a solution during the week
                                                as...
                                            </div>
                                            <!--div class="timestamp">
                                                Nov 05, 09:47
                                            </div-->
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Messages dropdown -->
            <!-- Global search (desktop and tablet only) -->
            <div class="navbar-item nav-icon search-icon modal-trigger" data-modal="search-modal">
                <i class="sl sl-icon-magnifier"></i>
            </div>
            <!-- /Global search -->

            <!-- Right sidebar trigger (desktop and tablet only) -->
            <div class="navbar-item nav-icon chat-button" data-show="quickview" data-target="main-quickview">
                <i class="im im-icon-Speach-Bubble11"></i>
            </div>
            <!-- /Right sidebar trigger -->
        </div>
    </div>
    <!-- /Nav right -->
</div>
