@extends('layout')

@section('content')

    @for($i = 0; $i < 2; $i++ )
        <div class="column is-4">
            <div class="flex-card light-bordered hover-inset">
                <div class="flex-card-header">
                    <div class="header-avatar skype-avatar">
                        <img src="https://randomuser.me/api/portraits/women/57.jpg">
                        <i class="fa fa-vk"></i>
                    </div>
                    <div class="header-info">
                        <div class="header-title">Helen Miller</div>
                        <div class="header-subtitle">Sales team</div>
                    </div>
                    <div class="header-control ml-auto">
                        <i class="sl sl-icon-arrow-down"></i>
                    </div>
                </div>
                <div class="profile-info-body">
                    <div class="info-section">
                        <div class="info-item birthday-info">
                            <i class="fa fa-birthday-cake"></i>
                            <span>12 сентября 1992</span>
                        </div>
                        <div class="info-item">
                            <div class="columns is-gapless">
                                <div class="column is-5"><div class="info-title">Возраст :</div></div>
                                <div class="column is-7"><div class="info-description highlighted">27</div></div>
                            </div>
                        </div>
                        <div class="profile-stats">
                            <div class="inline-stats has-text-centered">
                                <div class="stats-item">
                                    <div class="stat-name">
                                        Followers
                                    </div>
                                    <div class="stat-data">
                                        15K
                                    </div>
                                </div>
                                <div class="stats-item">
                                    <div class="stat-name">
                                        Following
                                    </div>
                                    <div class="stat-data">
                                        876
                                    </div>
                                </div>
                                <div class="stats-item">
                                    <div class="stat-name">
                                        Tweets
                                    </div>
                                    <div class="stat-data">
                                        1231
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="profile-footer">
                            <span class="tag squared is-outlined is-warning">Одноклассники</span>
                            <span class="tag squared is-outlined is-warning">Друзья</span>
                            <span class="tag squared is-outlined is-warning">Друзья</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endfor
@endsection
