@include('layout.header')

<!-- Main sidebar menu -->
<nav class="main-menu is-dark">
    @include('partials.sidebar-nav')
</nav>
<!-- /Main sidebar menu -->

<!-- Child push sidebar menu -->
<nav class="child-menu">
    @include('partials.child-sidebar-nav')
</nav>
<!-- /Child push sidebar menu -->

<!-- Top Navbar -->
<nav class="navbar dashboard-nav has-shadow">
    @include('partials.top-nav')
</nav>
<!-- /Top Navbar -->

<!-- Main dashboard container -->
<div id="dashboard-wrapper" class="columns">

    <div class="column"></div>
    <div class="content column is-11">
        <nav class="breadcrumbs">
            <ul>
                <li><a href="#">Home</a></li>
            </ul>
        </nav>

        <div class="dashboard-wrapper">
            <div class="columns">
                <div class="column">
                    <div id="main-dashboard" class="section-wrapper">
                        <div class="columns is-multiline dashboard-columns">
                            @yield('content')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="column"></div>

</div>
<!-- /Main dashboard container -->
<!-- /Main -->
@include('layout.footer')
